#include <systemc.h>
#include <string>
#include "reg_file.hpp"

using namespace std;

SC_MODULE(TestBench)
{
    sc_signal<bool> clock;
    sc_signal<sc_uint<1> > reset;
    sc_signal<sc_uint<1> > write_en;
    sc_signal<sc_uint<3> > target_addr;
    sc_signal<sc_uint<3> > source1_addr;
    sc_signal<sc_uint<3> > source2_addr;
    sc_signal<sc_uint<16> > target;
    sc_signal<sc_uint<16> > source1;
    sc_signal<sc_uint<16> > source2;
    reg_file RF;
    
    SC_CTOR(TestBench) : RF("RF")
    {
        SC_THREAD(clock_thread);
        SC_THREAD(stimulus_thread);
        RF.CLK(this->clock);
        RF.RST(this->reset);
        RF.WE_RF(this->write_en);
        RF.TGT_addr(this->target_addr);
        RF.SRC1_addr(this->source1_addr);
        RF.SRC2_addr(this->source2_addr);
        RF.TGT(this->target);
        RF.SRC1(this->source1);
        RF.SRC2(this->source2);
        init_values();
    }

    int check() 
    {

        for (unsigned i=0;i<TEST_SIZE;i++) 
        {
            if ((res1[i] != risultato_test[i])||(res2[i] != risultato_test[i]))
            {
                cout << "TEST FALLITO: " << i << endl;
                cout << "RISULTATO TEORICO: " << risultato_test[i] << endl;
                cout << "RISULTATO TEST (SRC1) : " << res1[i] << endl;
                cout << "RISULTATO TEST (SRC2) : " << res2[i] << endl;
                return 1;
             }
        }

        if (reset_ok==false)
        {
	  cout << "RESET FAIL" << endl;
          return 1;
        }

        if (cross_read_ok==false)
        {
	  cout << "ERRORE NELLA LETTURA DI DUE REGISTRI DIVERSI" << endl;
          return 1;
        }

        cout << "RESET OK, tutti i registri sono stati reinizializzati a zero" << endl << endl;
        cout << "CLOCK PERIOD : " << clock_period << " ns" << endl;
        cout << "CLOCK COUNT : " << clock_count/2 << endl;
        cout << "SIMULATION TIME : " << clock_period*clock_count/2 << " ns" << endl;
        cout << "TEST OK" << endl;                 
        return 0;
    }


  private:

   void clock_thread() 
   {

     bool value = false;
     while(true) 
     {
            clock.write(value);
            value = !value;
            clock_count++;
            wait(clock_period/2,SC_NS);
      }
   }

   void stimulus_thread() 
   { 
        //RESET iniziale
        reset.write(0);
        wait(clock_period,SC_NS);
        reset.write(1);
        wait(clock_period,SC_NS);
        cout << "START" << endl << endl;
        //Scrivo nel register file
        cout << "SCRITTURA NEL REGISTER FILE" << endl << endl;
        for (unsigned i=0;i<TEST_SIZE;i++) 
        {
            target_addr.write(target_addr_test[i]);
            target.write(target_test[i]);
            write_en.write(1);
            cout << "Indirizzo : " << target_addr_test[i];
            cout << " | Dato da scrivere : " << target_test[i] << endl;
            wait(clock_period,SC_NS);
            write_en.write(0);
            wait(clock_period,SC_NS);
        }
        //Leggo da register file
        cout << endl << "LETTURA DA REGISTER FILE" << endl << endl;
        for (unsigned k=0;k<TEST_SIZE;k++)
        {
            source1_addr.write(target_addr_test[k]);
            source2_addr.write(target_addr_test[k]);
            wait(clock_period,SC_NS);
            res1[k] = source1.read();
            res2[k] = source2.read();
            cout << "Registro da leggere : " << target_addr_test[k];
            cout << " | Source 1 : " << res1[k];
            cout << " | Source 2 : " << res2[k] << endl;
            wait(clock_period,SC_NS);
            
        }

        //Lettura incrociata
        cout << endl << "LETTURA DI DUE REGISTRI DIVERSI" << endl << endl;
        for (unsigned k=0;k<TEST_SIZE-1;k++)
        {
            source1_addr.write(target_addr_test[k]);
            source2_addr.write(target_addr_test[k+1]);
            wait(clock_period,SC_NS);
            cross1[k] = source1.read();
            cross2[k] = source2.read();
            cout << "Address 1 : " << target_addr_test[k];
            cout << " | Source 1 : " << cross1[k] << endl;
            cout << "Address 2 : " << target_addr_test[k+1];
            cout << " | Source 2 : " << cross2[k] << endl << endl;
            wait(clock_period,SC_NS);
            if ((cross1[k]!=target_test[k])||(cross2[k]!=target_test[k+1]))
	      cross_read_ok=false;
        }
        cout << "RESET" << endl << endl;
        //RESET
        reset.write(0);
        wait(5,SC_NS);
        reset.write(1);
        wait(5,SC_NS);
        //Verifico che il register file si sia resettato
        for (unsigned k=0;k<TEST_SIZE;k++)
        {
            source1_addr.write(target_addr_test[k]);
            source2_addr.write(target_addr_test[k]);
            wait(clock_period,SC_NS);
            if ((source1.read()!=0)||(source2.read()!=0))
               reset_ok=false;
            wait(clock_period,SC_NS);
        }    


    }

    static const unsigned TEST_SIZE = 8;
    // NOTA: short = 2 byte
    unsigned short target_addr_test[TEST_SIZE];
    unsigned short target_test[TEST_SIZE];
    unsigned short risultato_test[TEST_SIZE];
    unsigned short res1[TEST_SIZE];
    unsigned short res2[TEST_SIZE];
    unsigned short cross1[TEST_SIZE];
    unsigned short cross2[TEST_SIZE];
    bool reset_ok;
    bool cross_read_ok;
    unsigned clock_count;
    unsigned clock_period;
    void init_values() 
    {
       //Inizializzazione del contatore dei cicli di clock
       clock_count=0;
       //Periodo di clock in nanosecondi
       clock_period=20;
       reset_ok=true;
       cross_read_ok=true;
       target_addr_test[0]=0;
       target_addr_test[1]=1;
       target_addr_test[2]=2;
       target_addr_test[3]=3;
       target_addr_test[4]=4;
       target_addr_test[5]=5;
       target_addr_test[6]=6;
       target_addr_test[7]=7;

       target_test[0]=0;
       target_test[1]=49;
       target_test[2]=48;
       target_test[3]=47;
       target_test[4]=0;
       target_test[5]=1;
       target_test[6]=0;
       target_test[7]=66;

       risultato_test[0]=target_test[0];
       risultato_test[1]=target_test[1];
       risultato_test[2]=target_test[2];
       risultato_test[3]=target_test[3];
       risultato_test[4]=target_test[4];
       risultato_test[5]=target_test[5];
       risultato_test[6]=target_test[6];
       risultato_test[7]=target_test[7];

    }


};

int sc_main(int argc, char* argv[])
{
  int sim_time = 2000;

  TestBench test_RF("test_RF");

  sc_start(sim_time,SC_NS);

  return test_RF.check();
}
