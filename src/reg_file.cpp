#include <systemc.h>
#include "reg_file.hpp"

using namespace std;

void reg_file::update()
{
  //8 REGISTRI DA 16 bit
  sc_uint<16> R0;
  sc_uint<16> R1;
  sc_uint<16> R2;
  sc_uint<16> R3;
  sc_uint<16> R4;
  sc_uint<16> R5;
  sc_uint<16> R6;
  sc_uint<16> R7;
  R0=0;
  R1=0;
  R2=0;
  R3=0;
  R4=0;
  R5=0;
  R6=0;
  R7=0;
  cout << "REGISTER FILE INIZIALIZZATO A ZERO" << endl << endl;
  while(true)
  {
    //Aspetto la sensitivity: fronte di salita del clock oppure reset
    wait();
    //Se RST=0 scrivi 0 in tutti i registri
    if (RST->read()==0)
    {
     R0=0;
     R1=0;
     R2=0;
     R3=0;
     R4=0;
     R5=0;
     R6=0;
     R7=0;
    }
    else
    {
      //Se WE_RF=1 scrivo il dato presente sulla porta TGT nel registro indicato da TGT_addr
      if (WE_RF->read()==1)
      {
        switch(TGT_addr->read())
        {
           case 0 : R0=0;
                    //cout << "   Scritto " << R0 << " in R0" << endl;
                    break;
           case 1 : R1=TGT->read();
                    cout << "   R1 <= " << R1 << endl;
                    break;
           case 2 : R2=TGT->read();
                    cout << "   R2 <= " << R2 << endl;
                    break;
           case 3 : R3=TGT->read(); 
                    cout << "   R3 <= " << R3 << endl;
                    break;
           case 4 : R4=TGT->read();
                    cout << "   R4 <= " << R4 << endl;
                    break;
           case 5 : R5=TGT->read();
                    cout << "   R5 <= " << R5 << endl;
                    break;
           case 6 : R6=TGT->read();
                    cout << "   R6 <= " << R6 << endl;
                    break;
           case 7 : R7=TGT->read();
                    cout << "   R7 <= " << R7 << endl;
                    break;
        }
      }
      wait(5,SC_NS);
      //Scrivo sulla porta SRC1 il registro indicato da SRC1_addr
        switch(SRC1_addr->read())
        {
           case 0 : SRC1->write(R0);
                    //cout << "   SRC1 : letto " << R0 << " da registro 0" << endl;
                    break;
           case 1 : SRC1->write(R1);
                    //cout << "   SRC1 : letto " << R1 << " da registro 1" << endl;
                    break;
           case 2 : SRC1->write(R2);
                    //cout << "   SRC1 : letto " << R2 << " da registro 2" << endl;
                    break;
           case 3 : SRC1->write(R3);
                    //cout << "   SRC1 : letto " << R3 << " da registro 3" << endl;
                    break;
           case 4 : SRC1->write(R4); 
                    //cout << "   SRC1 : letto " << R4 << " da registro 4" << endl;
                    break;
           case 5 : SRC1->write(R5);
                    //cout << "   SRC1 : letto " << R5 << " da registro 5" << endl;
                    break;
           case 6 : SRC1->write(R6);
                    //cout << "   SRC1 : letto " << R6 << " da registro 6" << endl;
                    break;
           case 7 : SRC1->write(R7);
                    //cout << "   SRC1 : letto " << R7 << " da registro 7" << endl;
                    break;
        }
      //Scrivo sulla porta SRC2 il registro indicato da SRC2_addr
        switch(SRC2_addr->read())
        {
           case 0 : SRC2->write(R0);
                    //cout << "   SRC2 : letto " << R0 << " da registro 0" << endl;
                    break;
           case 1 : SRC2->write(R1);
                    //cout << "   SRC2 : letto " << R1 << " da registro 1" << endl;
                    break;
           case 2 : SRC2->write(R2); 
                    //cout << "   SRC2 : letto " << R2 << " da registro 2" << endl;
                    break;
           case 3 : SRC2->write(R3); 
                    //cout << "   SRC2 : letto " << R3 << " da registro 3" << endl;
                    break;
           case 4 : SRC2->write(R4); 
                    //cout << "   SRC2 : letto " << R4 << " da registro 4" << endl;
                    break;
           case 5 : SRC2->write(R5); 
                    //cout << "   SRC2 : letto " << R5 << " da registro 5" << endl;
                    break;
           case 6 : SRC2->write(R6); 
                    //cout << "   SRC2 : letto " << R6 << " da registro 6" << endl;
                    break;
           case 7 : SRC2->write(R7); 
                    //cout << "   SRC2 : letto " << R7 << " da registro 7" << endl;
                    break;
        }

      }  
  }
}
