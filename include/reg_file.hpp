#ifndef REG_FILE_HPP
#define REG_FILE_HPP

SC_MODULE(reg_file)
{
   //INGRESSI
   sc_in<bool>  CLK;
   sc_in<sc_uint<1> >   RST;
   sc_in<sc_uint<1> >   WE_RF;
   sc_in<sc_uint<3> >   TGT_addr;
   sc_in<sc_uint<3> >   SRC1_addr;
   sc_in<sc_uint<3> >   SRC2_addr;
   sc_in<sc_uint<16> >   TGT;
   // USCITE
   sc_out<sc_uint<16> >  SRC1;
   sc_out<sc_uint<16> >  SRC2;

   SC_CTOR(reg_file)
   {
     SC_THREAD(update);
     sensitive << CLK.pos() << RST;
   }
   private:
   void update ();
};


#endif
